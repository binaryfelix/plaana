require('dotenv').config();

const _ = require('lodash');
const path = require('path');
// eslint-disable-next-line no-unused-vars
const debug = require('debug')(`${'plaana'}:${path.basename(__filename).split('.')[0]}`);
const Profile = require('./model');

const omitProfileFields = Object.keys(_.omit(Profile.schema.paths, ['_id', '__v', 'deleted', 'professionalID']));

const update = (req, res) => {
  let profileFields = _.pick(req.body, omitProfileFields);
  // Get only fields that are not empty
  profileFields = _.pickBy(profileFields, _.identity);
  Profile.findOneAndUpdate({
    professionalID: req.user.id
  }, profileFields)
    .then(() => res.status(200).json({
      message: 'Profile updated'
    }))
    .catch(err => res.status(400).json(err.message));
};

const getProfile = (req, res) => {
  Profile.findOne({
    professionalID: req.user.id
  })
    .then((profile) => {
      const foundProfile = _.pick(profile, omitProfileFields);
      res.status(200).json(foundProfile);
    })
    .catch(err => res.status(400).json(err.message));
};

const deleteProfile = (req, res) => {
  Profile.findOneAndUpdate({
    professionalID: req.user.id
  }, {
    deleted: true
  })
    .then(() => res.status(200).json({
      message: 'Erased profile.'
    }))
    .catch(err => res.status(400).json(err.message));
};

const recoverProfile = (req, res) => {
  Profile.findOneAndUpdate({
    professionalID: req.user.id
  }, {
    deleted: false
  })
    .then(() => res.status(200).json({
      message: 'Recovered profile.'
    }))
    .catch(err => res.status(400).json(err.message));
};

const getOneProfile = (req, res) => {
  const profileToShow = req.params.id;
  Profile.findOne({ professionalID: profileToShow })
    .then((profile) => {
      const foundProfile = _.pick(profile, omitProfileFields);
      res.status(200).json(foundProfile);
    })
    .catch(err => res.status(400).json(err.message));
};

module.exports = {
  update,
  getProfile,
  deleteProfile,
  recoverProfile,
  getOneProfile
};
