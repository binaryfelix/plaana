const mongoose = require('mongoose');

const ProfileSchema = mongoose.Schema;

const profile = new ProfileSchema({
  deleted: {
    type: Boolean,
    default: false
  },
  professionalID: {
    type: ProfileSchema.Types.ObjectId,
    ref: 'User'
  },
  linkedIn: String,
  city: {
    type: String,
    required: true
  },
  professionalTrajectory: {
    type: String,
    required: true
  },
  abilities: {
    type: String,
    required: true
  },
  objectives: {
    type: String,
    required: true
  },
  academicBackground: {
    type: String,
    required: true
  },
  experience: {
    type: String,
    required: true
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

module.exports = mongoose.model('Profile', profile);
