const express = require('express');
const passport = require('passport');
const {
  isLoggedIn,
  isLoggedOut
} = require('../../authentication/auth');
const {
  signup,
  login,
  update,
  getUser,
  getOneUser,
  logout,
  activateAccount,
  sendVerificationMail,
  deleteAccount
} = require('./controller');

const server = express();

server.post('/signup', isLoggedOut, signup);
server.post('/login', isLoggedOut, passport.authenticate('local'), login);
server.post('/update', isLoggedIn, update);
server.get('/loggedUser', isLoggedIn, getUser);
server.get('/:id', isLoggedIn, getOneUser);
server.get('/logout', isLoggedIn, logout);
server.get('/confirm/:confirmationCode', activateAccount);
server.get('/verifyAccount', isLoggedIn, sendVerificationMail);
server.delete('/delete', isLoggedIn, deleteAccount);

module.exports = server;
