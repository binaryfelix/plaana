const passport = require('passport');

require('./serializers');
require('./localStrategy');

module.exports = (server) => {
  server.use(passport.initialize());
  server.use(passport.session());
};
