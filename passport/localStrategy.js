const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../api/user/model');

passport.use(
  new LocalStrategy((username, password, done) => {
    User.findOne({ email: username }, (err, foundUser) => {
      if (err) {
        return done(err);
      }
      if (!foundUser) {
        return done(null, false, { message: 'Incorrect username' });
      }
      if (!bcrypt.compareSync(password, foundUser.password)) {
        return done(null, false, { message: 'Incorrect password' });
      }
      return done(null, foundUser);
    });
  })
);
