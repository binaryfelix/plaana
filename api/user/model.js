const mongoose = require('mongoose');

// eslint-disable-next-line prefer-destructuring
const UserSchema = mongoose.Schema;

const user = new UserSchema({
  isActive: {
    type: Boolean,
    default: false
  },
  name: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  curriculumPath: String,
  curriculumName: String,
  profileImgPath: String,
  profileImgName: String,
  confirmationCode: {
    type: String,
    required: true
  },
  deleted: {
    type: Boolean,
    default: false
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

module.exports = mongoose.model('User', user);
