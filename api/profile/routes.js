const express = require('express');
const {
  isLoggedIn
} = require('../../authentication/auth');
const {
  update,
  recoverProfile,
  deleteProfile,
  getProfile,
  getOneProfile
} = require('./controller');

const server = express();

server.post('/update', isLoggedIn, update);
server.post('/recover', isLoggedIn, recoverProfile);
server.delete('/delete', isLoggedIn, deleteProfile);
server.get('/profile', isLoggedIn, getProfile);
server.get('/:id', isLoggedIn, getOneProfile);

module.exports = server;
