require('dotenv').config();

const path = require('path');
const logger = require('morgan');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const debug = require('debug')(`${'plaana'}:${path.basename(__filename).split('.')[0]}`);

const server = express();
const dbURL = process.env.DBURL;

mongoose
  .connect(dbURL, {
    useNewUrlParser: true
  })
  .then((x) => {
    debug(`Connected to Mongo! Database name: "${x.connections[0].name}"`);
  })
  .catch((err) => {
    debug('Error connecting to mongo', err);
  });

// Middleware Setup
server.use(logger(process.env.LOGGER));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({
  extended: false
}));

server.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 24 * 60 * 60 },
  store: new MongoStore({
    mongooseConnection: mongoose.connection,
    ttl: 24 * 60 * 60 // 1 day
  })
}));
require('./passport')(server);
require('./routes/index')(server);

server.use(function (req, res) {
  res.status(404).json({ message: 'Page not found' });
});

module.exports = server;
