const isLoggedIn = (req, res, next) => {
  if (req.user !== undefined) {
    // User is logged in
    next();
  } else {
    // User is logged out
    res.status(400).json({ message: 'You must login first' });
  }
};

const isLoggedOut = (req, res, next) => {
  if (req.user === undefined) {
    // User is logged out
    next();
  } else {
    // User is logged in
    res.status(400).json({ message: 'You must log out first' });
  }
};

const isActivated = (req, res, next) => {
  if (req.user.isActivated) {
    next();
  } else {
    res.status(400).json({ message: 'You must activate your account' });
  }
};

module.exports = {
  isLoggedIn,
  isLoggedOut,
  isActivated
};
