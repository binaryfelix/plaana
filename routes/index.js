const userRoutes = require('../api/user/routes');
const profileRoutes = require('../api/profile/routes');

module.exports = (server) => {
  server.use('/api/user', userRoutes);
  server.use('/api/profile', profileRoutes);
};
