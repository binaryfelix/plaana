require('dotenv').config();

const _ = require('lodash');
const path = require('path');
const bcrypt = require('bcrypt');
// eslint-disable-next-line no-unused-vars
const debug = require('debug')(`${'plaana'}:${path.basename(__filename).split('.')[0]}`);
const nodemailer = require('nodemailer');
const User = require('./model');
const Profile = require('../profile/model');

const userSchema = Object.keys(_.omit(User.schema.paths, ['_id', '__v', 'deleted']));
const omitUserFields = Object.keys(_.omit(User.schema.paths, ['_id', '__v', 'deleted', 'password', 'confirmationCode', 'isActive']));
const profileSchema = Object.keys(_.omit(Profile.schema.paths, ['_id', '__v', 'deleted']));
const bcryptSalt = parseInt(process.env.BCRYPT, 10);

const getUser = (req, res) => {
  User.findById(req.user.id)
    .then((user) => {
      const showUser = _.pick(user, omitUserFields);
      return res.status(200).json(showUser);
    })
    .catch(err => res.status(400).json(err.message));
};

const getOneUser = (req, res) => {
  const userId = req.params.id;
  User.findById(userId)
    .then((user) => {
      const showUser = _.pick(user, omitUserFields);
      res.status(200).json(showUser);
    })
    .catch(err => res.status(400).json(err.message));
};

const signup = (req, res) => {
  const userFields = _.pick(req.body, userSchema);
  const profileFields = _.pick(req.body, profileSchema);
  User.findOne({
    email: userFields.email
  }).then((user) => {
    if (user) {
      res.status(400).json({
        message: 'User already exists.'
      });
    } else {
      // Encrypt password and generate confirmation code to be verified via email.
      const salt = bcrypt.genSaltSync(bcryptSalt);
      const hashPass = bcrypt.hashSync(userFields.password, salt);
      const confirmationCode = encodeURIComponent(bcrypt.hashSync(userFields.email, salt)).match(/\w+/g).join('');
      userFields.password = hashPass;
      userFields.confirmationCode = confirmationCode;
      const newUser = new User(userFields);
      User.create(newUser, (err, createdUser) => {
        if (err) return res.status(400).json(err.message);
        return req.logIn(createdUser, (errLogin) => {
          if (errLogin) return res.status(400).json(errLogin.message);
          // Create the user's profile
          profileFields.professionalID = req.user.id;
          const newProfile = new Profile(profileFields);
          return Profile.create(newProfile, (errProfile) => {
            if (errProfile) {
              return User.findByIdAndRemove({
                _id: createdUser.id
              }).then(() => res.status(400).json(errProfile.message));
            }
            return res.status(200).json({
              message: 'User saved. Logged in automatically.'
            });
          });
        });
      });
    }
  });
};

const login = (req, res) => {
  const userInfo = req.user;
  const getUserFields = ['id', 'name', 'lastName', 'email'];
  const newUserInfo = _.pick(userInfo, getUserFields);
  res.status(200).json(newUserInfo);
};

const logout = (req, res) => {
  req.logout();
  res.status(200).json({
    message: 'User logged out.'
  });
};

const update = (req, res) => {
  let userFields = _.pick(req.body, userSchema);
  // Pick the userFields from the body that are filled. Excluding 'null' and 'undefined'.
  userFields = _.pickBy(userFields, _.identity);
  User.findById(req.user.id).then((user) => {
    // If the user is updating his email
    if (req.body.email && req.body.email !== user.email) {
      // User has to activate the account again
      userFields.isActive = false;
    }
    // If the user is updating his password, we have to verify that he knows his current password.
    if (req.body.newPassword) {
      const salt = bcrypt.genSaltSync(bcryptSalt);
      const currentHashPassword = bcrypt.hashSync(userFields.password, salt);
      const newHashPassword = bcrypt.hashSync(userFields.newPassword, salt);
      if (currentHashPassword === user.password) {
        userFields.password = newHashPassword;
        User.update(req.user.id, userFields).then(() => {
          res.status(200).json({
            message: 'User updated.'
          });
        }).catch(err => res.send(400).json(err.message));
      } else {
        res.status(400).json({
          message: 'Incorrect password.'
        });
      }
    } else {
      User.update({
        _id: req.user.id
      }, userFields)
        .then(() => res.status(200).json({
          message: 'User updated.'
        }))
        .catch(err => res.status(400).json(err.message));
    }
  }).catch(error => console.log(error));
};

const activateAccount = (req, res) => {
  User.findOne({
    confirmationCode: req.params.confirmationCode
  }).then((foundUser) => {
    if (foundUser === null) {
      return res.status(400).json({
        message: 'Could not find a correspondent activation code.'
      });
    }
    return foundUser.update({
      isActive: true,
      confirmationCode: ''
    }).then(() => res.status(200).json({
      message: 'Conta ativada!'
    }))
      .catch(err => res.status(400).json(err.message));
  });
};

const sendVerificationMail = (req, res) => {
  User.findById(req.user.id).then((foundUser) => {
    // configure nodemailer
    if (foundUser.confirmationCode === '') {
      return res.status(400).json({
        message: 'Could not find a correspondent activation code.'
      });
    }
    const {
      confirmationCode
    } = foundUser;
    const activationURL = `${
      process.env.BaseURL
    }/api/user/confirm/${confirmationCode}`;
    return nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: process.env.GMAILUSER,
        pass: process.env.GMAILPASS
      }
    })
      .sendMail({
        from: process.env.GMAILUSER,
        to: foundUser.email,
        subject: 'Ative sua conta Plaana!',
        html: `Ative sua conta Plaana <a href="${activationURL}">aqui!</a>`
      })
      .then(info => res.status(200).json(info))
      .catch(err => res.status(400).json(err));
  }).catch(err => err);
};

const deleteAccount = (req, res) => {
  User.findByIdAndUpdate({
    _id: req.user.id
  }, {
    deleted: true
  })
    .then(() => res.status(200).json({
      message: 'Erased account.'
    }))
    .catch(err => res.status(400).json(err.message));
};

module.exports = {
  getUser,
  getOneUser,
  signup,
  login,
  logout,
  update,
  activateAccount,
  sendVerificationMail,
  deleteAccount
};
